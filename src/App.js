import React, { Component } from 'react';
import NavBar from './Components/layouts/NavBar';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Dashboard from './Components/Dashboard/Dashboard';
import SinglePost from './Components/Posts/SinglePost';
import SignedIn from './Components/auth/SignedIn';
import SigneUp from './Components/auth/SigneUp';
import CreatePost from './Components/Posts/CreatePost';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <NavBar />
            <Switch>
              <Route exact path="/" component={Dashboard}/>
              <Route path="/post/:id" component={SinglePost}/>
              <Route path="/signedin" component={SignedIn}/>
              <Route path="/signedup" component={SigneUp}/>
              <Route path="/create-post" component={CreatePost}/>
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
