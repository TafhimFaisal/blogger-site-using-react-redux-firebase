import React, { Component } from "react";
import ProjectDetails from "./ProjectDetails";
import Notification from "./Notification";
import { connect } from "react-redux";

class Dashboard extends Component {
  render() {
    const  { projects }  = this.props;

    return (
      <div>
        <div className="dashboard container">
          <div className="row">
            <div className="col s12 m6">
              <ProjectDetails projects={projects} />
            </div>
            <div className="col s12 m5 offset-m1">
              <Notification />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    projects: state.Project.projects
  }
};

export default connect(mapStateToProps)(Dashboard);
