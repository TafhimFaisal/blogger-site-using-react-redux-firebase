import React, { Component } from 'react'
import ProjectSummery from '../Posts/ProjectSummery'

export default class ProjectDetails extends Component {
    ProjectCard = this.props.projects.map((project,index)=>{
            return <ProjectSummery content={project.content} id={project.id} title={project.title} key={index}/>
    })
    
    render() {
        
        return (
            <div>
                <div className="row">
                    {this.ProjectCard}
                </div>
            </div>
        )
    }

    

}
