import React, { Component } from 'react';
import { createProject } from '../action/ProjectAction';
import { connect } from  'react-redux';

export class CreatePost extends Component {

    state = {
        title:'',
        content:''
    }
    handleChange = (e) =>{
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleSubmit = (e) =>{
        e.preventDefault()
        this.props.createProject(this.state);
        //console.log(this.state);
    }


    render() {
        return (
            <div>
                <div className="container">
                    <form className="white" onSubmit={this.handleSubmit}>
                        <h5 className="grey-text text-darken-3">Create a New Project</h5>
                        <div className="input-field">
                            <input type="text" id='title' onChange={this.handleChange} />
                            <label htmlFor="title">Project Title</label>
                        </div>
                        <div className="input-field">
                            <textarea id="content" className="materialize-textarea" onChange={this.handleChange}></textarea>
                            <label htmlFor="content">Project Content</label>
                        </div>
                        <div className="input-field">
                            <button className="btn pink lighten-1">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

const mapCreatProjectToprops = (dispatch) => {
    return {
        createProject: (project) => dispatch(createProject(project))
    } 
}

export default connect(null,mapCreatProjectToprops)(CreatePost);
