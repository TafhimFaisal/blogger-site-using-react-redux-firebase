import React from 'react'
import { Link } from 'react-router-dom'
import SignedInLink from './SignedInLink'
import SignedOutLink from './SignedOutLink'


const NavBar = () => {
    return (

        <div>
            <nav className="nav-wrapper grey darken-3">
                <div className="container">
                    <Link to='/' className="brand-logo">MarioPlan</Link>
                    <SignedOutLink />
                    <SignedInLink />
                </div>
            </nav>
        </div>

    )
}

export default NavBar;