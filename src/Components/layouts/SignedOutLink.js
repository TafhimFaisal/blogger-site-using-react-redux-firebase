import React from 'react'
import { NavLink } from 'react-router-dom'


const SignedOutLink = () => {
    return (
        <div>
            <ul className="right">
                <li><NavLink to='/signedup'>Signup</NavLink></li>
                <li><NavLink to='/signedin'>Login</NavLink></li>
            </ul>
        </div>
    )
}

export default SignedOutLink
