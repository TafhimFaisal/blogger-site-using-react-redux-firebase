import AuthReducer from './AuthReducer';
import ProjectReducer from './ProjectReducer';
import { combineReducers } from 'redux';

const RootReducer = combineReducers({
    Auth:AuthReducer,
    Project:ProjectReducer
})

export default RootReducer;
